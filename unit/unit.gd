class_name Unit extends CharacterBody2D

## 是否选择了单位
@export var selected: bool = false:
	get:
		return selected
	set(value):
		selected = value
		_set_selected(value)
@onready var box: Panel = $Box as Panel
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D as CollisionShape2D
@onready var shape: Shape2D



func _ready() -> void:
	_set_selected(selected)
	

func _set_selected(value: bool) -> void:
	if is_instance_valid(box):
		box.visible = value


func _on_input_event(_viewport: Node, _event: InputEvent, _shape_idx: int) -> void:
	pass # Replace with function body.

func _process(_delta: float) -> void:
	pass
	

# 获取
func get_rect() -> Rect2:
	return shape.get_rect()
	
	
func get_global_rect() -> Rect2:
	var rect: Rect2 = get_rect()
	return Rect2(to_global(rect.position), rect.size)
